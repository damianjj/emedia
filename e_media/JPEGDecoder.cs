﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e_media
{
    class JPEGDecoder
    {
        public int Lenght { get; set; }
        public int DataPrecision { get; set; }
        public int ImageHeight { get; set; }
        public int ImageWidth { get; set; }

        public void ImageProperies()
        {
            const string Path = @"..\..\Images\image1.jpg"; 

            byte[] byteArray = File.ReadAllBytes(Path);
            
            for (int i = 0; i < (byteArray.Length); i++) //  byteArray iteration
            {
                if (byteArray[i] == 0xff && byteArray[i + 1] == 0xd8) // looking for start of image (SOI)
                {
                    Console.WriteLine("Start of Image was detected... ");
                }
                else if (byteArray[i] == 0xff && byteArray[i + 1] == 0xc0) // SOF0 marker indefier
                {
                    Lenght = (byteArray[i + 2]) * 256 + (byteArray[i + 3]);
                    DataPrecision = byteArray[i + 4];
                    ImageHeight = (byteArray[i + 5]) * 256 + (byteArray[i + 6]);
                    ImageWidth = (byteArray[i + 7]) * 256 + (byteArray[i + 8]);
                    int numberOfComponents = byteArray[i + 9];
                    int eachComponent_componentID = byteArray[i + 10];
                    int eachComponent_samplingFactors = byteArray[i + 11];
                    int eachComponent_quantizationTableNumber = byteArray[i + 12];
     
                    Console.WriteLine("JPG info: ");
                    Console.WriteLine("Lenght: " + Lenght);
                    Console.WriteLine("Data precision: " + DataPrecision);
                    Console.WriteLine("Height: " + ImageHeight);
                    Console.WriteLine("Width: " + ImageWidth);
                }
                else if (byteArray[i] == 0xff && byteArray[i + 1] == 0xd9) // looking for end of image (EOI)
                {
                    Console.WriteLine("End of Image was detected... ");
                }
            }
        }
    }
}