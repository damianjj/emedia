﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e_media
{
    class Program
    {
        static void Main(string[] args)
        {
            JPEGDecoder o = new JPEGDecoder();

            o.ImageProperies();

            OriginalImage.Image = Image.FromFile(@"..\..\Images\image2.jpg");

            Fourier fft = new Fourier((Bitmap)OriginalImage.Image);
            fft.ForwardFFT();
            fft.FFTShift();
            fft.FFTPlot(fft.FFTShifted);

            magnitudeImage.Image = (Image)fft.FourierPlot;
            phaseImage.Image = (Image)fft.PhasePlot;
            fft.InverseFFT();
            inverseImage.Image = fft.Obj;

            Console.ReadLine();
        }
    }
}
